﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace heatmap
{
    class Program
    {
        static void Main(string[] args)
        {
            string dataFile = System.IO.File.ReadAllText(@"\\profile01\FRW7\kraevayaa\Desktop\heatmap\heatmap\SMS_DC_DataFile_074.csv");
            dataFile = dataFile.Replace('\n','\r');
            string[] lines = dataFile.Split(new[] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries);
            int totalRows = lines.Length;
            int totalCols = lines[0].Split(';').Length;

            string[,] resultVals = new string[totalRows, totalCols];

            for (int row = 0; row < totalRows; row++)
            {
                string[] line_r = lines[row].Split(';');
                for (int col = 0; col < totalCols; col++)
                {
                    resultVals[row,col] = line_r[col];
                    string value = resultVals[row,col];
                    if (value[0] == '-')
                        resultVals[row,col] = "20.0";
                }
            }

            Console.WriteLine("columns = "+totalCols);
            Console.WriteLine("rows = " + totalRows);
            
            

           /* for (int row = 0; row < totalRows; row++)
            {
                Console.WriteLine();
                for (int col = 0; col < totalCols; col++)
                {
                    Console.Write(resultVals[row,col]+" ");
                }
            }*/

            //int[] numberOfColumns = new int[54];

            // для левой стороны
            List<int> numberOfColumns1 = new List<int>();

            int start = 47; 
            for (int i = 1; i <= 463; i+=33)
            {
                numberOfColumns1.Add(start);
                numberOfColumns1.Add(start + 2);
                numberOfColumns1.Add(start + 6);
                start += 64;
            }
           

            for (int i = 1; i <= 19; i += 9)
            {
                numberOfColumns1.Add(start);
                numberOfColumns1.Add(start + 2);
                numberOfColumns1.Add(start + 6);
                start += 16;
            }

            int end = Convert.ToInt32(Console.ReadLine());
            //int start1 = end - 720 - 1; // 3 minutes
            //int start1 = end - 1400 - 1;
            int start1 = end - 1200 - 1; // 5 минут до стикера
            string str1 = "";

            System.IO.StreamWriter file1 = new System.IO.StreamWriter(@"\\profile01\FRW7\kraevayaa\Desktop\testheatmap\SMS_DC_DataFile_074_1\outputLeftSide.txt");

            for (int i = start1; i <= end; i += 4)
            {
                //Console.Write("[");
                str1 += "[";
                int count = 0;
                for (int j = 0; j < numberOfColumns1.Count; j++)
                {
                    if (count == 3)
                        count = 0;

                    if (count == 0)
                        // Console.Write("[");
                        str1 += "[";
                    if (count == 2)
                    {
                        //Console.Write(resultVals[i, numberOfColumns[j]]);
                        str1 += (resultVals[i, numberOfColumns1[j]]).ToString();
                        if (j == numberOfColumns1.Count - 1)
                            //Console.Write("]");
                            str1 += "]";
                        else
                            // Console.Write("], ");
                            str1 += "], ";
                    }
                    else
                    {
                        //Console.Write(resultVals[i, numberOfColumns[j]] + ", ");
                        str1 += (resultVals[i, numberOfColumns1[j]]).ToString();
                        str1 += ", ";
                    }
                    count++;
                }
                //Console.Write("]");
                if (i == end-1)
                    str1 += "]";
                else
                    str1 += "],";
                file1.WriteLine(str1);
                file1.WriteLine();
                str1 = "";
                //Console.WriteLine();
                //Console.WriteLine();
            }
            file1.Close();



            // для правой стороны
            List<int> numberOfColumns2 = new List<int>();

            int start2 = 79;
            for (int i = 1; i <= 463; i += 33)
            {
                numberOfColumns2.Add(start2);
                numberOfColumns2.Add(start2 + 2);
                numberOfColumns2.Add(start2 + 6);
                start2 += 64;
            }

            start2 = start2 - 24;

            for (int i = 1; i <= 19; i += 9)
            {
                numberOfColumns2.Add(start2);
                numberOfColumns2.Add(start2 + 2);
                numberOfColumns2.Add(start2 + 6);
                start2 += 16;
            }

            string str2 = "";

            System.IO.StreamWriter file2 = new System.IO.StreamWriter(@"\\profile01\FRW7\kraevayaa\Desktop\testheatmap\SMS_DC_DataFile_074_1\outputRightSide.txt");

            for (int i = start1; i <= end; i += 4)
            {
                str2 += "[";
                int count = 0;
                for (int j = 0; j < numberOfColumns2.Count; j++)
                {
                    if (count == 3)
                        count = 0;

                    if (count == 0)
                        str2 += "[";
                    if (count == 2)
                    {
                        str2 += (resultVals[i, numberOfColumns2[j]]).ToString();
                        if (j == numberOfColumns2.Count - 1)
                            str2 += "]";
                        else
                            str2 += "], ";
                    }
                    else
                    {
                        str2 += (resultVals[i, numberOfColumns2[j]]).ToString();
                        str2 += ", ";
                    }
                    count++;
                }
               // Console.WriteLine(i);
                if (i == end-1)
                    str2 += "]";
                else
                    str2 += "],";
                file2.WriteLine(str2);
                file2.WriteLine();
                str2 = "";
            }
            file2.Close();


            // для задней стороны
            List<int> numberOfColumns3 = new List<int>();

            int start3 = 87;
            for (int i = 1; i <= 463; i += 33)
            { 
                numberOfColumns3.Add(start3);
                numberOfColumns3.Add(start3 + 2);
                numberOfColumns3.Add(start3 + 4);
                numberOfColumns3.Add(start3 + 6);
                numberOfColumns3.Add(start3 + 8);
                numberOfColumns3.Add(start3 + 10);
                numberOfColumns3.Add(start3 + 14);
                numberOfColumns3.Add(start3 + 16);
                numberOfColumns3.Add(start3 + 18);
                numberOfColumns3.Add(start3 + 20);
                numberOfColumns3.Add(start3 + 22);
                start3 += 64;
            }

            //Console.WriteLine(num);
            //Console.ReadKey();
           /* for (int i = 0; i < numberOfColumns3.Count; i++ )
            {
                Console.WriteLine(numberOfColumns3[i]);
            }*/

            //Console.WriteLine(numberOfColumns3.Count);
            string str3 = "";


            System.IO.StreamWriter file3 = new System.IO.StreamWriter(@"\\profile01\FRW7\kraevayaa\Desktop\testheatmap\SMS_DC_DataFile_074_1\outputFixedSide.txt");

            for (int i = start1; i <= end; i += 4)
            {
                str3 += "[";
                int count = 0;
                for (int j = 0; j < numberOfColumns3.Count; j++)
                {
                    if (count == 11)
                        count = 0;

                    if (count == 0)
                        str3 += "[";
                    if (count == 10)
                    {
                        str3 += (resultVals[i, numberOfColumns3[j]]).ToString();
                        if (j == numberOfColumns3.Count - 1)
                            str3 += "]";
                        else
                            str3 += "], ";
                    }
                    else
                    {
                        str3 += (resultVals[i, numberOfColumns3[j]]).ToString();
                        str3 += ", ";
                    }
                    count++;
                }
                if (i == end-1)
                    str3 += "]";
                else
                    str3 += "],";
                file3.WriteLine(str3);
                file3.WriteLine();
                str3 = "";
            }
            file3.Close();


            // для передней стороны
            List<int> numberOfColumns4 = new List<int>();

            int start4 = 55;
            for (int i = 1; i <= 463; i += 33)
            {
                numberOfColumns4.Add(start4);
                numberOfColumns4.Add(start4 + 2);
                numberOfColumns4.Add(start4 + 4);
                numberOfColumns4.Add(start4 + 6);
                numberOfColumns4.Add(start4 + 8);
                numberOfColumns4.Add(start4 + 10);
                numberOfColumns4.Add(start4 + 14);
                numberOfColumns4.Add(start4 + 16);
                numberOfColumns4.Add(start4 + 18);
                numberOfColumns4.Add(start4 + 20);
                numberOfColumns4.Add(start4 + 22);
                start4 += 64;
            }

            string str4 = "";

            System.IO.StreamWriter file4 = new System.IO.StreamWriter(@"\\profile01\FRW7\kraevayaa\Desktop\testheatmap\SMS_DC_DataFile_074_1\outputLooseSide.txt");

            for (int i = start1; i <= end; i += 4)
            {
                str4 += "[";
                int count = 0;
                for (int j = 0; j < numberOfColumns4.Count; j++)
                {
                    if (count == 11)
                        count = 0;

                    if (count == 0)
                        str4 += "[";
                    if (count == 10)
                    {
                        str4 += (resultVals[i, numberOfColumns4[j]]).ToString();
                        if (j == numberOfColumns4.Count - 1)
                            str4 += "]";
                        else
                            str4 += "], ";
                    }
                    else
                    {
                        str4 += (resultVals[i, numberOfColumns4[j]]).ToString();
                        str4 += ", ";
                    }
                    count++;
                }
                if (i == end-1)
                    str4 += "]";
                else
                    str4 += "],";
                file4.WriteLine(str4);
                file4.WriteLine();
                str4 = "";
            }

            file4.Close();
            // Console.ReadKey();

        }
    }
}