﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace parsing
{
    class Program
    {
        static void ParsingForLeftAndRightSides(string nameOfOutputFile, int numberOfRows, string[][] resultVals, int totalRows, int start, string numberFile)
        {
            string str = ""; int p = 0; 
            using (var sw = new StreamWriter(nameOfOutputFile, false, Encoding.Default))
            {
                for (int i = 0; i < totalRows; i++)
                {
                    str = ""; p = start;
                    for (int j = 0; j < numberOfRows; j++)
                    {
                        str += resultVals[i][p];
                        str += ";";
                        str += resultVals[i][p + 1];
                        str += ";";
                        str += resultVals[i][p + 3];
                        if (j != numberOfRows - 1)
                            str += ";";
                        else
                        {
                            str += ";";
                            //str += resultVals[i][resultVals[0].Length - 1];
                        }

                        if ((nameOfOutputFile == @"\\profile01\FRW7\kraevayaa\Desktop\parsingtestfiles\SMS_DC_DataFile_RightSide_" + numberFile) && (j == numberOfRows - 4))
                        {
                            p += 20;
                        }
                        else
                        {
                            if ((j == numberOfRows - 2) || (j == numberOfRows - 3))
                                p += 8;
                            else
                                p += 32;
                        }
                    }
                    sw.WriteLine(str);
                }
            }
        }

        static void ParsingForLooseAndFixedSides(string nameOfOutputFile, int numberOfRows, string[][] resultVals, int totalRows, int start)
        {
            string str = ""; int p = 0;
            using (var sw = new StreamWriter(nameOfOutputFile, false, Encoding.Default))
            {
                for (int i = 0; i < totalRows; i++)
                {
                    str = ""; p = start;
                    for (int j = 0; j < numberOfRows; j++)
                    {
                        str += resultVals[i][p];
                        str += ";";
                        str += resultVals[i][p + 1];
                        str += ";";
                        str += resultVals[i][p + 2];
                        str += ";";
                        str += resultVals[i][p + 3];
                        str += ";";
                        str += resultVals[i][p + 4];
                        str += ";";
                        str += resultVals[i][p + 5];
                        str += ";";
                        str += resultVals[i][p + 7];
                        str += ";";
                        str += resultVals[i][p + 8];
                        str += ";";
                        str += resultVals[i][p + 9];
                        str += ";";
                        str += resultVals[i][p + 10];
                        str += ";";
                        str += resultVals[i][p + 11];

                        if (j != numberOfRows - 1)
                            str += ";";
                        else
                        {
                            str += ";";
                            //str += resultVals[i][resultVals[0].Length - 1];
                        }
                        
                        p += 32;
                    }
                    sw.WriteLine(str);
                }
            }
        }

        static void Main(string[] args)
        {


            string[] filePaths = Directory.GetFiles(@"\\profile01\FRW7\kraevayaa\Desktop\parsing\parsing\files\", "*.csv");
            for (int k1 = 0; k1 < filePaths.Length; k1++)
            {
                //string dataFile = System.IO.File.ReadAllText(@"\\profile01\FRW7\kraevayaa\Desktop\parsing\parsing\SMS_DC_DataFile_079.csv");
                string dataFile = System.IO.File.ReadAllText(@filePaths[k1]);
                string numberOfFile = filePaths[k1].Substring(filePaths[k1].Length - 7, 7);
                Console.WriteLine(numberOfFile);
                dataFile = dataFile.Replace('\n', '\r');
                string[] lines = dataFile.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                int totalRows = lines.Length;
                int totalCols = lines[0].Split(';').Length;
                int countOfColumns = 505-1;

                string[][] resultVals = new string[totalRows][];

                for (int row = 0; row < totalRows; row++)
                {
                    string[] line_columns = lines[row].Split(';');
                    int k = 0;
                    resultVals[row] = new string[countOfColumns];
                    for (int col = 47; col < totalCols; col += 2)
                    {
                        resultVals[row][k] = line_columns[col];
                        k++;
                    }
                    //resultVals[row][k] = line_columns[25];
                }


                int numberOfRows = 18;
                int numberOfFirstColumn = 0;
                string nameOfOutputFile = @"\\profile01\FRW7\kraevayaa\Desktop\parsingtestfiles\SMS_DC_DataFile_LeftSide_" + numberOfFile;
                ParsingForLeftAndRightSides(nameOfOutputFile, numberOfRows, resultVals, totalRows, numberOfFirstColumn, numberOfFile);

                numberOfFirstColumn = 16;
                nameOfOutputFile = @"\\profile01\FRW7\kraevayaa\Desktop\parsingtestfiles\SMS_DC_DataFile_RightSide_" + numberOfFile;
                ParsingForLeftAndRightSides(nameOfOutputFile, numberOfRows, resultVals, totalRows, numberOfFirstColumn, numberOfFile);


                numberOfFirstColumn = 20; numberOfRows = 15;
                nameOfOutputFile = @"\\profile01\FRW7\kraevayaa\Desktop\parsingtestfiles\SMS_DC_DataFile_FixedSide_" + numberOfFile;
                ParsingForLooseAndFixedSides(nameOfOutputFile, numberOfRows, resultVals, totalRows, numberOfFirstColumn);


                numberOfFirstColumn = 4;
                nameOfOutputFile = @"\\profile01\FRW7\kraevayaa\Desktop\parsingtestfiles\SMS_DC_DataFile_LooseSide_" + numberOfFile;
                ParsingForLooseAndFixedSides(nameOfOutputFile, numberOfRows, resultVals, totalRows, numberOfFirstColumn);
            }
        }
    }
}