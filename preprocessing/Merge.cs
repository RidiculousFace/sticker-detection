﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {

           string nameOfOutputFile = @"\\profile01\FRW7\kraevayaa\Desktop\ConsoleApplication2\SMS_DC_DataFile_Minus3.csv";
            using (var sw = new StreamWriter(nameOfOutputFile, false, Encoding.Default))
            {
                string[] filePaths = Directory.GetFiles(@"\\profile01\FRW7\kraevayaa\Desktop\ConsoleApplication2\ConsoleApplication2\files\", "*.csv");
                for (int k = 0; k < filePaths.Length; k++)
                {
                    //Console.WriteLine(filePaths.Length);
                    string dataFile = System.IO.File.ReadAllText(@filePaths[k]);
                    string numberOfFile = filePaths[k].Substring(filePaths[k].Length - 7, 3);
                    //string numberOfFile = filePaths[k].Substring(59, filePaths[k].Length-59);
                    //string number = numberOfFile.Substring(0, 3);
                    Console.WriteLine(numberOfFile);
                    dataFile = dataFile.Replace('\n', '\r');
                    string[] lines = dataFile.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    int totalRows = lines.Length;
                    int totalCols = lines[0].Split(';').Length;
                    string[][] resultVals = new string[totalRows][];
                    Console.WriteLine("Total rows = " + totalRows);

                    for (int row = 0; row < totalRows; row++)
                    {
                        string[] line_columns = lines[row].Split(';');
                        resultVals[row] = new string[totalCols];
                        for (int col = 0; col < totalCols; col += 1)
                        {
                            resultVals[row][col] = line_columns[col];
                            /*if (line_columns[col].Contains(","))
                                Console.WriteLine("File consists of , " + numberOfFile);*/
                        }
                    }

                    string str = ""; int p = 0;
                    if ((numberOfFile == "012") || (numberOfFile == "039") || (numberOfFile == "059") || (numberOfFile == "079"))
                        p = 10759;
                    else
                        p = 10800;
                    

                    for (int row = 0; row < totalRows/4; row++)
                    {
                        str = "";
                        for (int j = 0; j < 4; j++)
                        {
                            int firstRow = p * j + row;
                            for (int col = 0; col < totalCols; col += 1)
                            {
                                if ((col == totalCols - 1) && (j == 3))
                                {
                                    str += resultVals[firstRow][col];
                                    //str += ";";
                                }
                                else
                                {
                                    str += resultVals[firstRow][col];
                                    str += ";";
                                }
                            }
                        }
                        sw.WriteLine(str);
                    }

                }
                Console.ReadKey();
            }
        }
    }
}
