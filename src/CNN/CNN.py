import pandas as pd
import numpy as np
import os
import glob
from numpy import array
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import f1_score

#initialization
X_train = []
Y_train = []
labels = []

tmp_train_plus = [1]
tmp_train_minus = [0]

X_test = []
Y_test = [[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[0],[1],[1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0]]

class_weight = {0 : 1.,
    1: 50.}

lst_plus = ['SMS_DC_DataFile_Plus.csv']
lst_minus = ['SMS_DC_DataFile_Minus.csv']

#reading csv

#plus
for file_ in lst_plus:
	tmp = []
	for df in pd.read_csv(file_, sep=';', header = None, chunksize=10000):
		a = df.values
		for chunk in a:
			tmp = np.reshape(chunk, (-1, 11, 1))
			#tmp = tmp[np.newaxis, ...]
			X_train.append(tmp)
			Y_train.append(tmp_train_plus)
			#labels.append(1)
#minus
for file_ in lst_minus:
	tmp = []
	for df in pd.read_csv(file_, sep=';', header = None, chunksize=10000):
		a = df.values
		for chunk in a:
			tmp = np.reshape(chunk, (-1, 11, 1))
			#tmp = tmp[np.newaxis, ...]
			X_train.append(tmp)
			Y_train.append(tmp_train_minus)
			#labels.append(0)

#cast and print train data shapes
X_train = array(X_train)
Y_train = array(Y_train)
print(X_train.shape)
print(Y_train.shape)


#reading test data
tmp = []
for df in pd.read_csv('Test.csv', sep=';', header=None, chunksize=10000):
	a = df.values
	for chunk in a:
		tmp = np.reshape(chunk, (-1, 11, 1))
		#tmp = tmp[np.newaxis, ...]
		X_test.append(tmp)
#cast and print test data shapes
X_test = array(X_test)
Y_test = array(Y_test)
print(X_test.shape)
print(Y_test.shape)

#imports for CNN
from keras.models import Model
from keras.layers import Input, Convolution2D, MaxPooling2D, GlobalMaxPooling2D, Dense, Dropout, Flatten
from keras.utils import np_utils

#settings
batch_size = 25
num_epochs = 10
kernel_size = 3
pool_size = 2 
conv_depth_1 = 32 
conv_depth_2 = 64 
drop_prob_1 = 0.25
drop_prob_2 = 0.5 
hidden_size = 32

depth = 1
height = 72
width = 11
num_classes = 2

#cast to float type and normalize
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= np.max(X_train) # Normalise X_train to [0, 1] range
X_test /= np.max(X_test) # Normalise X_train to [0, 1] range

#highlight two classes
Y_train = np_utils.to_categorical(Y_train, num_classes) # One-hot encode the labels
#Y_test = np_utils.to_categorical(Y_test, num_classes) # One-hot encode the labels

#create model func
def create_model():
	inp = Input(shape=(height, width, depth))
	conv_1 = Convolution2D(conv_depth_1, kernel_size, kernel_size, border_mode='same', activation='relu')(inp)
	conv_2 = Convolution2D(conv_depth_1, kernel_size, kernel_size, border_mode='same', activation='relu')(conv_1)
	pool_1 = MaxPooling2D(pool_size=(pool_size, pool_size), strides=(2,2))(conv_2)
	#conv_3 = Convolution2D(conv_depth_1, kernel_size, kernel_size, border_mode='same', activation='relu')(pool_1)
	#pool_2 = GlobalMaxPooling2D()(pool_1)
	flatten = Flatten()(pool_1)
	drop_1 = Dropout(drop_prob_1)(flatten)
	#hidden = Dense(hidden_size, activation='relu')(drop_1)
	out = Dense(num_classes, activation='softmax')(drop_1)
	model = Model(input=inp, output=out)
	model.compile(loss='categorical_crossentropy', 
              optimizer='sgd',
              metrics=['accuracy'])
	return model

#train, (evaluate), predict func
def train_and_evaluate(model, X_train, Y_train, X_test, Y_test):

	model.fit(X_train, Y_train,batch_size=batch_size, \
		nb_epoch=num_epochs, verbose=1, class_weight=class_weight, validation_split=0.1)
	#model.fit(X_train, Y_train,batch_size=batch_size, \
        #        nb_epoch=num_epochs, verbose=1)
	result = model.predict(X_test, verbose=1)
	print(model.summary())
	print('Finished')
	
	np.savetxt('result.txt', result)
	#print(result)

#main contains K-Fold cross-validation
#from sklearn we could done the run-time augmentation with fit-generator, but...
if __name__ == "__main__":
	#This is the Stratified K-Fold cross-validation
	#We use it to estimate the neural network power in prediction
	#and to tuning it.
	'''
	n_folds = 10
	skf = StratifiedKFold(labels, n_folds=n_folds, shuffle=True)	

	for i, (train, test) in enumerate(skf):
		print ("Running Fold", i+1, "/", n_folds)
		model = None
		model = create_model()
		train_and_evaluate(model, X_train[train], Y_train[train],X_train[test], Y_train[test])
	'''
	#work with model
	model = create_model()
	print(model.summary())
	train_and_evaluate(model, X_train, Y_train, X_test, Y_test)