Values of sticker detection should be normalized.
Well, divide the values in second column on max_value in second column.
E.g.: 
ex = [[1,2]
      [2,3]
      [3,4]
      [3,8]]
Normalized: 
ex = [[1, 2/8]
      [2, 3/8]
      [3, 4/8]
      [3, 8/8]